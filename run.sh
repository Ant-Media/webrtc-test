BASEDIR=$(dirname "$0")
cd $BASEDIR


CP="webrtc-load-test.jar:libs/*"
NATIVE=-Djava.library.path=libs/native
MAIN=io.antmedia.Starter

java -cp $CP $NATIVE $MAIN $@
