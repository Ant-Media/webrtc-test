mvn clean install -Dmaven.javadoc.skip=true -Dmaven.test.skip=true -Dgpg.skip=true -P assemble

OUT=$?

if [ $OUT -ne 0 ]; then
    exit $OUT
fi

SRC=target/webrtc-load-test*SNAPSHOT.jar

if [ -d ~/test/webrtc-load-test/ ]
then
    DEST=~/test/webrtc-load-test/webrtc-load-test.jar
    cp $SRC $DEST
    if [ $? -eq 0 ]; then
        echo "File '$SRC' has been successfully copied to '$DEST'"
    else
        echo "Failed to copy file '$SRC' to '$DEST'"
    fi
fi



