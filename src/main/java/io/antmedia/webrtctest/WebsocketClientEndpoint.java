package io.antmedia.webrtctest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.websocket.ClientEndpoint;
import javax.websocket.ClientEndpointConfig;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.glassfish.tyrus.client.ClientManager;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.webrtc.IceCandidate;
import org.webrtc.SessionDescription;
import org.webrtc.SessionDescription.Type;

import io.antmedia.websocket.WebSocketConstants;


@ClientEndpoint
public class WebsocketClientEndpoint {

	private static Logger logger = LoggerFactory.getLogger(WebsocketClientEndpoint.class);
	private JSONParser jsonParser = new JSONParser();
	WebSocketController webSocketController;
	private Session session;
	private URI uri;
	private ClientManager websocketClient;
	private Settings settings;

	public WebsocketClientEndpoint(Settings settings) {
		String unsecure = "ws://"+settings.webSockAdr+":"+settings.port+"/"+settings.appName+"/websocket";
		String secure = "wss://"+settings.webSockAdr+":"+settings.port+"/"+settings.appName+"/websocket";
		try {
			uri = new URI(settings.isSequre ? secure : unsecure);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		this.settings = settings;
	}

	public void connect() {
		try {
			websocketClient = (ClientManager) ContainerProvider.getWebSocketContainer();
			websocketClient.asyncConnectToServer(this, uri);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void close() {
		websocketClient.shutdown();
		if (this.session != null && this.session.isOpen()) {
			try {
				this.session.close(new CloseReason(CloseReason.CloseCodes.NORMAL_CLOSURE, "Closing"));
			} catch (IOException e) {
				logger.error(ExceptionUtils.getStackTrace(e));
			}
		}
				
	}
	
	
	@OnOpen
	public void onOpen(Session session, EndpointConfig config)
	{
		logger.info("websocket opened {}", this.hashCode());
		this.session = session;
		
		
		if (webSocketController != null) {
			webSocketController.webSocketOpened();
		}
	}

	@OnClose
	public void onClose(Session session) {
		logger.info("websocket closed {}", this.hashCode());
		if (webSocketController != null) {
			webSocketController.webSocketClosed();
		}
	}

	@OnError
	public void onError(Session session, Throwable throwable) {
		logger.info("websocket onError hash:{}", this.hashCode());
	}

	@OnMessage
	public void onMessage(Session session, String message) {
		try {

			if (message == null) {
				logger.error("Received message null for session id: {}" , session.getId());
				return;
			}

			JSONObject jsonObject = (JSONObject) jsonParser.parse(message);

			String cmd = (String) jsonObject.get(WebSocketConstants.COMMAND);
			if (cmd == null) {
				logger.error("Received message does not contain any command for session id: {}" , session.getId());
				return;
			}

			final String streamId = (String) jsonObject.get(WebSocketConstants.STREAM_ID);
			
			if ((streamId == null || streamId.isEmpty()) &&
					!cmd.equals(WebSocketConstants.PONG_COMMAND) &&
					!cmd.equals(WebSocketConstants.ERROR_COMMAND)) 
			{
				logger.error("Incoming message:{}" , message);
				return;
			}
			
			if (cmd.equals(WebSocketConstants.PONG_COMMAND)) {
				webSocketController.pongMessageReceived();
			}
			
			if (cmd.equals(WebSocketConstants.START_COMMAND))  
			{
				webSocketController.createOffer();
			}
			else if (cmd.equals(WebSocketConstants.TAKE_CONFIGURATION_COMMAND))  
			{
				processTakeConfigurationCommand(jsonObject, session.getId(), streamId);
			}
			else if (cmd.equals(WebSocketConstants.TAKE_CANDIDATE_COMMAND)) 
			{
				processTakeCandidateCommand(jsonObject, session.getId(), streamId);
			}
			else if (cmd.equals(WebSocketConstants.STOP_COMMAND)) {
				
			}
			else if (cmd.equals(WebSocketConstants.PLAY_FINISHED)) {
				logger.info("play finished received from websocket {}", this.hashCode());
				webSocketController.stop();
			}
			else if (cmd.equals(WebSocketConstants.ERROR_COMMAND)) {
				logger.error("Incoming error message:{}" , message);
				String definition = (String) jsonObject.get(WebSocketConstants.DEFINITION);
				        
				if (WebSocketConstants.HIGH_RESOURCE_USAGE.equals(definition) 
						|| WebSocketConstants.NOT_INITIALIZED_YET.equals(definition)
						|| (WebSocketConstants.NO_STREAM_EXIST.equals(definition) && settings.mainTrack == null)
						|| WebSocketConstants.DATA_STORE_NOT_AVAILABLE.equals(definition)
						|| WebSocketConstants.MAINTRACK_DB_OPERATION_FAILED.equals(definition)
						|| WebSocketConstants.PUBLISH_TIMEOUT_ERROR.equals(definition)
						) 
				{
					//Set high resource usage to true to let the process try again
					logger.info("Error received. It will try again. Error is {}", definition);
					webSocketController.setTryagain(true);

				}
				else {
					logger.error("------------------------------------------------------");
					logger.error("Error received and it will not try again. Error is {}", definition);
					logger.error("------------------------------------------------------");

				}
			}
			else if (cmd.equals(WebSocketConstants.NOTIFICATION_COMMAND)) {

				String definition = (String) jsonObject.get(WebSocketConstants.DEFINITION);
				if (definition.equals(WebSocketConstants.JOINED_THE_ROOM)) 
				{
					webSocketController.joinedTheRoom();
				}
				else if (definition.equals(WebSocketConstants.PLAY_STARTED)) {
					webSocketController.playStarted();
				}
			}
			else if (cmd.equals(WebSocketConstants.STREAM_INFORMATION_NOTIFICATION)) {
			}
			else if (cmd.equals(WebSocketConstants.PUBLISH_STARTED)) {
			}
			else if (cmd.equals(WebSocketConstants.PONG_COMMAND)) {
			}
			
			else {
				logger.info("Undefined incoming message:{} ", message);
			}
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	public  void sendSDPConfiguration(String description, String type, String streamId) {

		sendMessage(getSDPConfigurationJSON (description, type,  streamId).toJSONString());
	}

	@SuppressWarnings("unchecked")
	public  void sendPublishStartedMessage(String streamId) {

		JSONObject jsonObj = new JSONObject();
		jsonObj.put(WebSocketConstants.COMMAND, WebSocketConstants.NOTIFICATION_COMMAND);
		jsonObj.put(WebSocketConstants.DEFINITION, WebSocketConstants.PUBLISH_STARTED);
		jsonObj.put(WebSocketConstants.STREAM_ID, streamId);

		sendMessage(jsonObj.toJSONString());
	}

	@SuppressWarnings("unchecked")
	public  void sendPublishFinishedMessage(String streamId) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put(WebSocketConstants.COMMAND, WebSocketConstants.NOTIFICATION_COMMAND);
		jsonObject.put(WebSocketConstants.DEFINITION,  WebSocketConstants.PUBLISH_FINISHED);
		jsonObject.put(WebSocketConstants.STREAM_ID, streamId);

		sendMessage(jsonObject.toJSONString());
	}

	@SuppressWarnings("unchecked")
	public  void sendStartMessage(String streamId) 
	{
		JSONObject jsonObject = new JSONObject();
		jsonObject.put(WebSocketConstants.COMMAND, WebSocketConstants.START_COMMAND);
		jsonObject.put(WebSocketConstants.STREAM_ID, streamId);

		sendMessage(jsonObject.toJSONString());
	}



	@SuppressWarnings("unchecked")
	protected  final  void sendNoStreamIdSpecifiedError()  {
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put(WebSocketConstants.COMMAND, WebSocketConstants.ERROR_COMMAND);
		jsonResponse.put(WebSocketConstants.DEFINITION, WebSocketConstants.NO_STREAM_ID_SPECIFIED);
		sendMessage(jsonResponse.toJSONString());	
	}

	@SuppressWarnings("unchecked")
	protected  final  void sendPlay(String streamId)  {
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put(WebSocketConstants.COMMAND, WebSocketConstants.PLAY_COMMAND);
		jsonResponse.put(WebSocketConstants.STREAM_ID, streamId);
		sendMessage(jsonResponse.toJSONString());	
	}

	@SuppressWarnings("unchecked")
	protected  final  void sendPublish(String streamId)  {
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put(WebSocketConstants.COMMAND, WebSocketConstants.PUBLISH_COMMAND);
		jsonResponse.put(WebSocketConstants.STREAM_ID, streamId);
		jsonResponse.put(WebSocketConstants.STREAM_NAME, streamId);
		jsonResponse.put(WebSocketConstants.VIDEO, !settings.audioOnly);
		jsonResponse.put(WebSocketConstants.AUDIO, true);
		if(settings.mainTrack != null) {
			jsonResponse.put(WebSocketConstants.META_DATA,
					"{\"isScreenShared\":false,\"isMicMuted\": false,\"isCameraOn\": true}");
			jsonResponse.put(WebSocketConstants.MAIN_TRACK, settings.mainTrack);
			jsonResponse.put(WebSocketConstants.ROLE, "default");
		}
		String jsonString = jsonResponse.toJSONString();
		logger.info("sending publish message -> {}", jsonString);
		sendMessage(jsonString);	
	}

	@SuppressWarnings("unchecked")
	public void sendTakeCandidateMessage(long sdpMLineIndex, String sdpMid, String sdp, String streamId)
	{
		sendMessage(getTakeCandidateJSON(sdpMLineIndex, sdpMid, sdp, streamId).toJSONString());
	}


	@SuppressWarnings("unchecked")
	public void sendMessage(String message) {
		synchronized (this) {
			if (session.isOpen()) {
				try {
					session.getBasicRemote().sendText(message);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			else {
				logger.error("WebSocket session is not open to send the message:{}", message);
			}
		}
	}


	public static JSONObject getTakeCandidateJSON(long sdpMLineIndex, String sdpMid, String sdp, String streamId) {

		JSONObject jsonObject = new JSONObject();
		jsonObject.put(WebSocketConstants.COMMAND,  WebSocketConstants.TAKE_CANDIDATE_COMMAND);
		jsonObject.put(WebSocketConstants.CANDIDATE_LABEL, sdpMLineIndex);
		jsonObject.put(WebSocketConstants.CANDIDATE_ID, sdpMid);
		jsonObject.put(WebSocketConstants.CANDIDATE_SDP, sdp);
		jsonObject.put(WebSocketConstants.STREAM_ID, streamId);

		return jsonObject;
	}

	public static JSONObject getSDPConfigurationJSON(String description, String type, String streamId) {

		JSONObject jsonResponseObject = new JSONObject();
		jsonResponseObject.put(WebSocketConstants.COMMAND, WebSocketConstants.TAKE_CONFIGURATION_COMMAND);
		jsonResponseObject.put(WebSocketConstants.SDP, description);
		jsonResponseObject.put(WebSocketConstants.TYPE, type);
		jsonResponseObject.put(WebSocketConstants.STREAM_ID, streamId);

		return jsonResponseObject;
	}

	private void processTakeConfigurationCommand(JSONObject jsonObject, String sessionId, String streamId) {
		String typeString = (String)jsonObject.get(WebSocketConstants.TYPE);
		String sdpDescription = (String)jsonObject.get(WebSocketConstants.SDP);

		SessionDescription.Type type;
		if (typeString.equals("offer")) {
			type = Type.OFFER;
		}
		else {
			type = Type.ANSWER;
		}


		SessionDescription sdp = new SessionDescription(type, sdpDescription);
		webSocketController.setRemoteDescription(sdp);
	}

	private void processTakeCandidateCommand(JSONObject jsonObject, String sessionId, String streamId) {
		String sdpMid = (String) jsonObject.get(WebSocketConstants.CANDIDATE_ID);
		String sdp = (String) jsonObject.get(WebSocketConstants.CANDIDATE_SDP);
		long sdpMLineIndex = (long)jsonObject.get(WebSocketConstants.CANDIDATE_LABEL);

		IceCandidate iceCandidate = new IceCandidate(sdpMid, (int)sdpMLineIndex, sdp);
		webSocketController.addIceCandidate(iceCandidate);

	}

	public void setWebSocketController(WebSocketController manager) {
		this.webSocketController = manager;
	}

	public void sendPingMessage() {
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put(WebSocketConstants.COMMAND, WebSocketConstants.PING_COMMAND);
		sendMessage(jsonResponse.toJSONString());	
	}

	public void sendJoinTheRoom(String streamId, String roomId, String multiTrack) {
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put(WebSocketConstants.COMMAND, WebSocketConstants.JOIN_ROOM_COMMAND);
		jsonResponse.put(WebSocketConstants.STREAM_ID, streamId);
		jsonResponse.put(WebSocketConstants.ROOM, roomId);
		jsonResponse.put(WebSocketConstants.MODE, multiTrack);
		sendMessage(jsonResponse.toJSONString());	
	}

	public void sendEnableTrack(String streamId, boolean b) 
	{
		//sending enable track 
		logger.info("sending enable track message for streamId:{} and enabled:{}", streamId, b);
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put(WebSocketConstants.COMMAND, WebSocketConstants.ENABLE_TRACK);
		jsonResponse.put(WebSocketConstants.STREAM_ID, streamId);
		jsonResponse.put(WebSocketConstants.TRACK_ID, streamId);
		jsonResponse.put(WebSocketConstants.ENABLED, b);
		sendMessage(jsonResponse.toJSONString());	
		
	}

}