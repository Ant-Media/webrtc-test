package io.antmedia.webrtctest;

public interface IWebRTCEventListerner {
	public void onCompleted(WebRTCClientEmulator webRTCClientEmulator, boolean connected);
	public void onDataChannelMessage(String string);
}
