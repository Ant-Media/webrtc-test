package io.antmedia.webrtctest;

public enum Mode {
	PLAYER,
	PUBLISHER, 
	PARTICIPANT,
	/*
	 * This is the mode that data channel is used. It sends play request to the server and disable video and audio
	 * After that it sends data messages for every 5 seconds
	 */
	DATA_PLAYER
}
