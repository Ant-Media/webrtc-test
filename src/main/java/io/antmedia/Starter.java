package io.antmedia;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.antmedia.webrtctest.FileReader;
import io.antmedia.webrtctest.IWebRTCEventListerner;
import io.antmedia.webrtctest.Mode;
import io.antmedia.webrtctest.Settings;
import io.antmedia.webrtctest.StatManager;
import io.antmedia.webrtctest.WebRTCClientEmulator;
import io.antmedia.webrtctest.WebRTCPlayer;
import io.antmedia.webrtctest.WebRTCPublisher;
import io.antmedia.webrtctest.WebSocketController;

public class Starter implements IWebRTCEventListerner
{
	private static Logger logger = LoggerFactory.getLogger(Starter.class);

	private Queue<WebSocketController> websocketControllers = new ConcurrentLinkedQueue<>();
	private FileReader reader;
	
	StatManager statManager;
    Settings settings = new Settings();
    
    private IWebRTCEventListerner listener;
	
	public Starter(String[] args) 
	{
		logger.info("~~~~~~~~ Start ({}) ~~~~~~~~", hashCode());
		settings.parse(args);
		statManager = new StatManager(settings.kafkaBrokers);
		if(settings.mode == Mode.PUBLISHER || settings.mode == Mode.PARTICIPANT) {
			reader = new FileReader(settings);
			if(reader.init()) {
				reader.start();
			}
			else {
				System.exit(1);
			}
		}
		ScheduledExecutorService executorService = null;
		

		for (int i = 0; i < settings.load; i++) 
		{
			//TODO: According to the test results, the number below can be increased
			if (i % 2 == 0) {
				executorService = Executors.newScheduledThreadPool(1);
			}
			String suffix = (settings.mode == Mode.PUBLISHER || settings.mode == Mode.PARTICIPANT) && settings.load > 1 ? "-"+i : ""; 
			
			WebSocketController webRTCManager = new WebSocketController(settings.streamId+suffix, settings, executorService);

			WebRTCClientEmulator webRTCClient = null;
			if(settings.mode == Mode.PUBLISHER || settings.mode == Mode.PARTICIPANT) 
			{
				webRTCClient = new WebRTCPublisher(reader, settings.loop);
			}
			else if(settings.mode == Mode.PLAYER || settings.mode == Mode.DATA_PLAYER)
			{
				webRTCClient = new WebRTCPlayer(settings);
			}

			if (webRTCClient == null) 
			{
				throw new IllegalArgumentException("Illegal mode not publisher or player");
			}
			
			webRTCClient.setScheduledExecutorService(executorService);
			webRTCClient.setManager(webRTCManager);
			webRTCManager.setWebRTCClient(webRTCClient);
			

			webRTCManager.setListener(this);

			getManagers().add(webRTCManager);
		}
		
		
		statManager.start();

	}

	public void start() {
		for (WebSocketController webSocketController : websocketControllers) {
			webSocketController.start();
		}
		
	}


	public static void main( String[] args )
	{
		Starter starter = new Starter(args);
		starter.start();
		
	}

	public void stop() {
		statManager.stop();
		for (WebSocketController webSocketController : getManagers()) {
			webSocketController.stop();
		}
		getManagers().clear();
		logger.info("~~~~~~~~ Stop ({})~~~~~~~~", hashCode());
	}
	
	public boolean isStopped() {
		for (WebSocketController webRTCManager : getManagers()) {
			if (webRTCManager.isStopped()) {
				return true;
			}
		}
		return false;
	}
	
	public IWebRTCEventListerner getListener() {
		return listener;
	}

	public void setListener(IWebRTCEventListerner listener) {
		this.listener = listener;
	}
	
	public void sendDataChannelMessage(String message) {
		getManagers().peek().sendDataChannelMessage(message);
	}

	@Override
	public void onCompleted(WebRTCClientEmulator webRTCClientEmulator, boolean connected) {
		logger.info("onCompleted is called for client:{} connected:{}", webRTCClientEmulator.hashCode(), connected);
		
		statManager.addStreamManager(webRTCClientEmulator);

		
	}
	
	@Override
	public void onDataChannelMessage(String string) {
	}

	public Queue<WebSocketController> getManagers() {
		return websocketControllers;
	}

	public void setManagers(Queue<WebSocketController> managers) {
		this.websocketControllers = managers;
	}
	

}
