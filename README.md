## WebRTCTest Tool
WebRTCTest tool is a java project for testing Ant Media Server webrtc capabilities.

* WebRTCTest is compatiple with Ant Media Server signalling protocol. 
* There is 3 modes of WebRTCTest: publisher, player, data. (-m flag determines the mode)
  - publisher: This mode pushes the WebRTC stream to the Ant Media Server. You also need to define the file to send
  - player: This mode plays the WebRTC stream that is live in the Ant Media Server
  - dataPlayer: This mode joins the streams to play and it will disable audio/video tracks and send test message every 5 seconds
  -  
* WebRTCTest two options with UI or without UI. (-u flag determines the UI od/off)
* You can save received(in player mode) or published(in publisher mode) frames as png. (-p flag determines the saving periof of frames. If you set 100 it saves every 100 frames.)

## Run
It can be run from command prompt with the following options.

### Publish a WebRTC stream
Following command publishes test.mp4 to the server(10.10.175.53) with `myStream`(default stream id)  and the default app `WebRTCAppEE` by using 5080 port and WS(WebSocket)  
```

./run.sh -f test.mp4 -m publisher -n 1 -s 10.10.175.53  
```

test.mp4 is H264 & Opus encoded file. You can use test.mp4 in the directory. 
While it publishes the stream, it will not transcode. This is why it's efficient to send many streams
 
Following command publishes test.mp4 to your {YOUR_SERVER_ADDRESS} with `myStream`(default stream id) and to the `LiveApp` through WSS(WebSocket Secure) using 5443 port.
```

./run.sh -f test.mp4 -m publisher -n 1 -s {YOUR_SERVER_ADDRESS} -q true -p 5443 -a LiveApp 
``` 


### Play a stream with WebRTC 
```
./run.sh -m player -n 100 -i stream1 -s 10.10.175.53 -u false #plays 100 viewers for default stream myStream
```



### Parameters
```
Flag 	 Name      	  				  Default   	         Description                 
---- 	 ----      	 				  -------   	         -----------   
f    	 File Name 	  				  test.mp4           Source file* for publisher output file for player        
s    	 Server IP or Domain name 	  localhost 	         server ip                   
q    	 Use WSS or WS    			  false     	         true(wss) or false(ws)      
l        Log Level      			      3           	     0:VERBOSE,1:INFO,2:WARNING,3:ERROR,4:NONE
i    	 Stream Id 	  				  myStream  	 		 id for stream               
m    	 Mode      	  				  player    	         publisher or player         
u    	 Show GUI  	  				  false              true or false               
p    	 Port      	  				  5080      	         websocket port number 
v    	 Verbose   	  				  false     	         true or false 
n    	 Count     	  				  1         	         Number of player/publisher connctions 
k        Kafka Broker  				  null        	     Kafra broker address with port
r    	 Publish Loop  				  false       	     true or false
c    	 Codec         				  h264        	     h264, VP8 or h265 
d    	 Data Channel  				  true       	     true or false 
a        Application name              WebRTCAppEE        Name of the application that will be used
t        MainTrack                     Null               id for maintrack   
```

*File should be in mp4 format and h264, opus encoded